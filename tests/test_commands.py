"""Test bot commands."""
import unittest
from unittest import mock

from freezegun import freeze_time

from irc_bot import irc_commands


class TestShuffledMembers(unittest.TestCase):
    """Test shuffled_members."""

    def test_shuffled_members(self):
        """Test shuffled_members."""
        cases = (
            ('2010-01-01', 'foo bar baz', None, 'baz, foo, bar'),
            ('2010-01-04', 'foo bar baz', None, 'foo, baz, bar'),
            ('2010-01-01', 'foo bar baz', 'foo', 'baz, bar'),
            ('2010-01-01', 'bar baz', 'foo', 'baz, bar'),

        )
        for time, members, remove, expected in cases:
            with freeze_time(time), mock.patch('irc_bot.irc_commands.TEAM_MEMBERS', members):
                for _ in range(10):
                    self.assertEqual(irc_commands.shuffled_members(remove=remove), expected)
