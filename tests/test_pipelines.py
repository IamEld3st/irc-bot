"""Pipeline handling tests."""
import unittest
from unittest import mock

from irc_bot import amqp


@mock.patch.dict('os.environ', {'URL_SHORTENER_URL': ''})
@mock.patch('irc_bot.amqp.IRC_PUBLISH_QUEUE', mock.Mock())
class TestPipeline(unittest.TestCase):
    """ Test webhooks."""

    def test_no_jobs_retrigger(self):
        """Check that retriggered empty pipelines are handled correctly."""
        payload = {
            "object_kind": "pipeline",
            "object_attributes": {
                "id": 612710, "stages": [], "status": "failed", "variables": []
            },
            "project": {"web_url": "url"},
            "commit": {
                "message": "Retrigger: retrigger-brew\n"
                "title = Retrigger: retrigger-brew\n"
                "subject = Retrigger: Brew: Task 46865701\n"
                "retrigger = true",
            },
            "builds": []
        }
        result = amqp.process_message(body=payload, headers={'message-type': 'gitlab'})
        self.assertEqual(result, [])

    def test_scratch_builds(self):
        """Check that Brew scratch builds are handled correctly."""
        payload = {
            'object_kind': 'pipeline',
            'object_attributes': {
                'id': 612710, 'stages': ['test'], 'status': 'failed',
                'variables': [
                    {'key': 'scratch', 'value': 'True'},
                ]
            },
            'builds': [{
                'name': 'test',
                'stage': 'test',
                'status': 'failed',
                'created_at': '2000-01-01',
                'id': 1,
            }],
            'project': {'web_url': 'url'},
        }
        result = amqp.process_message(body=payload, headers={'message-type': 'gitlab'})
        self.assertEqual(result, [])

    def test_no_jobs(self):
        """Check that pipelines without jobs are handled correctly."""
        payload = {
            "object_kind": "pipeline",
            "object_attributes": {
                "id": 612710, "stages": [], "status": "failed", "variables": []
            },
            "project": {"web_url": "url"},
            "commit": {
                "message": "Message\n"
                "title = Title\n"
                "subject = Brew: Task 46865701\n"
            },
            "builds": []
        }
        result = amqp.process_message(body=payload, headers={'message-type': 'gitlab'})
        self.assertEqual(len(result), 1)

    def test_dont_report(self):
        """Check that pipelines with valid failures are not reported."""
        payload = {
            "object_kind": "pipeline",
            "object_attributes": {
                "id": 612710, "stages": ["prepare", "merge", "kernel-results"],
                "status": "failed", "variables": []
            },
            "project": {"web_url": "url"},
            "commit": {
                "message": "Message\n"
                "title = Title\n"
                "subject = baseline run\n"
            },
            # Put the builds in the wrong order on purpose to make sure the
            # correct last job is found.
            "builds": [
                {"stage": "merge", "status": "failed", "id": 12},
                {"stage": "prepare", "status": "success", "id": 11},
                {"stage": "kernel-results", "status": "skipped", "id": 13}
            ]
        }
        result = amqp.process_message(body=payload, headers={'message-type': 'gitlab'})
        self.assertEqual(len(result), 0)

    def _issue(self, action, *, message_expected=True):
        payload = {
            "object_kind": "issue",
            "user": {
                "username": "username",
            },
            "project": {
                "path_with_namespace": "path_with_namespace",
            },
            "object_attributes": {
                "action": action,
                "iid": "iid",
                "state": "state",
                "title": "title",
                "url": "url",
            },
        }
        result = amqp.process_message(body=payload, headers={'message-type': 'gitlab'})
        if message_expected:
            self.assertEqual(len(result), 1)
            self.assertIn('username', result[0])
            self.assertIn('path_with_namespace', result[0])
            self.assertIn('iid', result[0])
            self.assertIn('state', result[0].lower())
            self.assertIn('title', result[0])
            self.assertIn('url', result[0])
        else:
            self.assertEqual(len(result), 0)

    def test_issue_opened(self):
        """Check that notifications about opening issues are shown."""
        self._issue('open')

    def test_issue_updated(self):
        """Check that notifications about updating issues are not shown."""
        self._issue('update', message_expected=False)

    def test_issue_closed(self):
        """Check that notifications about closing issues are shown."""
        self._issue('close')

    def test_sentry(self):
        """Check that sentry alerts are handled correctly."""
        payload = {
            'action': 'triggered',
            'data': {'event': {
                'title': 'title',
                'url': 'https://sentry.io/api/0/projects/o/project/events/1/',
                'web_url': 'https://web_url',
            }}
        }
        result = amqp.process_message(body=payload, headers={'message-type': 'sentry'})
        self.assertEqual(len(result), 1)
        self.assertIn('title', result[0])
        self.assertIn('project', result[0])
        self.assertIn('web_url', result[0])

    def test_sentry_ignored(self):
        """Check that sentry issues are ignored."""
        payload = {
            'action': 'created',
            'data': {'issue': {}}
        }
        result = amqp.process_message(body=payload, headers={'message-type': 'sentry'})
        self.assertEqual(len(result), 0)

    def test_irc_message(self):
        """Check that irc messages are handled correctly."""
        payload = {
            'message': 'message',
        }
        result = amqp.process_message(body=payload, headers={'message-type': 'irc'},
                                      routing_key='irc.message')
        self.assertEqual(len(result), 1)
        self.assertIn('message', result[0])
