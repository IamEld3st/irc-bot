# irc-bot

IRC bot hooked up to print pipeline status and notifications

## Deployment guide

The bot is meant to run as a service in a container (see the provided
Dockerfile). Please note that the URL shorteners are internal to Red Hat
infrastructure and will not work elsewhere -- feel free to fork the bot and use
your own ones.

## Environment variables

Add these to your deployment, all
are mandatory for the bot to work. If you don't need parts of the
functionality, feel free to comment out the related code.

* `GITLAB_PRIVATE_TOKEN`: Private token of the bot, used to edit list of
successes in the GitLab snippet.

* `GITLAB_SNIPPET`: URL of the GitLab snippet with a list of successes.

* `IRC_SERVER`: URL of the IRC server to connect to.

* `IRC_PORT`: Port of the IRC server to connect to.

* `IRC_CHANNEL`: Name of the channel to connect to.
